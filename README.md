# README #

This is a simple GeoDjango project. The admin interface is ready to use. The source code is based on [geodjango tutorial](https://docs.djangoproject.com/en/1.8/ref/contrib/gis/tutorial/). Before to start:

* be sure to know [django](https://docs.djangoproject.com/en/1.8/intro/) and [geodjango](https://docs.djangoproject.com/en/1.8/ref/contrib/gis/tutorial/) fundamentals.
* This webapp is built with a PostgreSQL database and the PostGis extension; be sure to check the [requirements](https://docs.djangoproject.com/en/1.8/ref/contrib/gis/install/postgis/).


# Copyright (C) 2016 - Alessandro Trinca Tornidor 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/

