#from django.contrib import admin
from django.contrib.gis import admin
from models import WorldBorder

# Register your models here.

## standard GIS admin interface
#admin.site.register(WorldBorder, admin.GeoModelAdmin)

# OSMGeoAdmin GIS interface (add an OpenStreetMap layer on the map)
admin.site.register(WorldBorder, admin.OSMGeoAdmin)