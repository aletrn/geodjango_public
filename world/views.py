from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    context_dict = {'vartext':"this is a variable from index view"}
    return render(request, 'world/index.html', context_dict)
    #return HttpResponse("geodjango index page: view")